import requests
import json
import datetime
import os
import hashlib

hash_md5 = hashlib.md5()

api_endpoint = input("\nAPI endpoint URL (default:https://gitlab.com/api/v4): ") or "https://gitlab.com/api/v4"
group_id = input("\nMaster GitLab Group ID: ") or "7180393"
project_name = input("\nMaster GitLab Project Slug: ") or "cs1ct-python-a"
deadline_input = input("\nDeadline (format: YYYY/MM/DD,DD:MM:SS): ")

if len(deadline_input) == 0:
  deadline_input = datetime.datetime.now()
  deadline = deadline_input
else:
  deadline_items = [(deadline_input.split(",")[0]).split("/"),(deadline_input.split(",")[1]).split(":")]
  deadline = datetime.datetime(int(deadline_items[0][0]), int(deadline_items[0][1]), int(deadline_items[0][2]), int(deadline_items[1][0]), int(deadline_items[1][1]), int(deadline_items[1][2]))

gitlab_access_token = input("\nGitLab API Access Token: ") or "rfYg_ovNcM9-3KwTWnqD"
request_headers = {"PRIVATE-TOKEN":gitlab_access_token}
verify_ci_file = open("custom-configs/verify/%s/.gitlab-ci.yml" % project_name, "rb").read()
verify_checksum = hashlib.md5(verify_ci_file).hexdigest()

summary = {}

if not os.path.exists(project_name+"/results/") and not os.path.exists(project_name+"/status/") and not os.path.exists(project_name+"/summary/"):
    os.makedirs(project_name+"/results/")
    os.makedirs(project_name+"/status/")
    os.makedirs(project_name+"/summary/")
    os.makedirs(project_name+"/warnings/")
subgroup_request = requests.get(api_endpoint+"/groups/"+group_id+"/subgroups/", headers=request_headers)
subgroup_response = subgroup_request.json()

for student in subgroup_response:
  student_id = student["name"]
  group_request = requests.get(api_endpoint+"/groups/"+str(student["id"]), headers=request_headers)
  project_request = requests.get(api_endpoint+"/groups/"+str(student["id"])+"/projects/"+"?"+"search="+project_name, headers=request_headers)
  project_response = project_request.json()
  pipeline_request = requests.get(api_endpoint+"/projects/"+str(project_response[0]["id"])+"/pipelines/"+"?"+"ref="+"master"+"&"+"updated_before="+str(deadline), headers=request_headers)
  latest_pipeline_response = pipeline_request.json()[0]
  jobs_request = requests.get(api_endpoint+"/projects/"+str(project_response[0]["id"])+"/pipelines/"+str(latest_pipeline_response["id"])+"/jobs/", headers=request_headers)
  jobs_response = jobs_request.json()

  student_ci_file = requests.get(api_endpoint+"/projects/"+str(project_response[0]["id"])+"/repository/"+"files/"+".gitlab-ci.yml/"+"raw?"+"ref="+"master", headers=request_headers)
  student_ci_file_checksum = hashlib.md5(student_ci_file.content).hexdigest()

  if verify_checksum != student_ci_file_checksum:
    ci_warnings_file = open(project_name+"/warnings/"+"ci_changes.txt", "a")
    ci_warnings_file.write(student_id)
    ci_warnings_file.close()

  stages_status = []
  stages = []
  statuses = []

  for job in jobs_response:
    stages_status.append({"stage": str(job["stage"]), "status": str(job["status"])})
    if str(job["stage"]) not in summary:
      if str(job["status"]) == "success":
        summary[str(job["stage"])] = {"pass" : 1, "fail" : 0}
      else:
        summary[str(job["stage"])] = {"pass" : 0, "fail" : 1}
    else:
      if str(job["status"]) == "success":
        summary[str(job["stage"])]["pass"] = summary[str(job["stage"])]["pass"] + 1
      else:
        summary[str(job["stage"])]["fail"] = summary[str(job["stage"])]["fail"] + 1

  final_job = jobs_response[len(jobs_response)-1]
  final_job_id = str(jobs_response[len(jobs_response)-1]["id"])
  artifact_request = requests.get(api_endpoint+"/projects/"+str(project_response[0]["id"])+"/jobs/"+final_job_id+"/artifacts/results/"+project_name+"/"+student_id+".txt", headers=request_headers)
  open(project_name+"/results/"+student_id+".txt", "ab").write(artifact_request.content)

  result_file = open(project_name+"/results/"+student_id+".txt", "a")
  for job in stages_status:
    result_file.write("STAGE: "+job["stage"]+" -> "+"STATUS: "+job["status"]+"\n")
    stages.append(job["stage"])
    statuses.append(job["status"])
  result_file.write("\n")
  result_file.close()

  status_file = open(project_name+"/status/"+student_id+".csv", "w")
  status_file.write(','.join(stages))
  status_file.write("\n")
  status_file.write(','.join(statuses))
  status_file.close()

  # students_summary_file.write("student,%s\n" % ','.join(stages))
  students_summary_file = open(project_name+"/summary/"+project_name+"_students"+".csv", "a")
  students_summary_file.write(student_id+","+",".join(statuses)+"\n")
  students_summary_file.close()

summary_file = open(project_name+"/summary/"+project_name+"_summary"+".csv", "w")
summary_file.write("stage,pass,fail\n")
for stage in summary.keys():
  summary_file.write(stage+","+str(summary[stage]["pass"])+","+str(summary[stage]["fail"])+"\n")
summary_file.close()
