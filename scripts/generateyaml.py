import json
import datetime
import os

script_path = os.path.dirname(os.path.realpath(__file__))
current_path = os.getcwd()
template_path = current_path+"/templates/"
language = input("Choose language for porject: (python | java)")

if language != "python" and language != "java":
  print("Only Python and Java suppoted currently")
  exit()

custom_path = current_path+"/custom-configs/"
verify_path = custom_path+"verify/"
project_slug = input("GitLab Project Slug (default: course-assignment-num): ") or "course-assignment-num"


if not os.path.exists(custom_path):
    os.makedirs(custom_path)
    os.makedirs(verify_path+project_slug)

place_holders = ["${PROJECT_NAME}$", "${TESTS_PATH}$", "${RESULTS_PATH}$", "${STATIC_FILES}$"]

template_ci_setup = open(template_path+language+"/"+language+"-setup"+".yml", "r")
output_ci_file = open(custom_path+project_slug+".yml", "w")

template_ci_data = template_ci_setup.read()

default_replacements = [project_slug, "src/tests", "results", "/home/ubuntu/gittutor"]
input_msgs = ["GitLab Project Slug (default: %s): " % project_slug, "Tests relative directory in project (default: src/tests): ", 
              "Results relative directory in project (default: results): ", "Server Static files directory for hidden files (default: /home/ubuntu/gittutor)"]

for itemnum in range(1,len(default_replacements)):
  response = input(input_msgs[itemnum])
  if len(response) != 0:
    default_replacements[itemnum] = response

for itemnum in range(len(place_holders)):
  template_ci_data = template_ci_data.replace(place_holders[itemnum], default_replacements[itemnum])

template_ci_setup.close()

if language == "python":
  stages = ["prechecks", "modules", "syntax", "tests", "hidden"]
elif language == "java":
  stages = ["prechecks", "compile", "tests"]

approved_stages = []

print("Please choose which pre-configured stages you would like to include in the custom CI:\n (all stages included by default)\n")

for stage in stages:
  response = input(stage + " stage: (y/n)")
  if response != "n":
    approved_stages.append(stage)

for stage in approved_stages:
  template_ci_stage = open(template_path+language+"/"+language+"-"+stage+".yml", "r")
  template_ci_data = template_ci_data + "\n" + template_ci_stage.read()
  template_ci_stage.close()

output_ci_file.write(template_ci_data)

output_ci_file.close()

verify_template = open(template_path+"verify/"+"verify.yml", "r")
verify_final = open(verify_path+project_slug+"/"+".gitlab-ci.yml", "w")
verify_final.write(verify_template.read())
verify_template.close()
verify_final.close()

