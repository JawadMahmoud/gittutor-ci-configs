# Usage Requirements

The system has a few requirements that should be met before starting to use it.

## Install Git

- Git is used as the primary Version Control System and is an essential component of this system

## Install Python 3

- The scripts of the system are implemented using Python 3.
- Python is also required for installing and using RepoBee.

## Install RepoBee

- Kindly visit RepoBee's official website and follow their [guidelines](https://repobee.readthedocs.io/en/stable/install.html) on installing RepoBee on your machine. This can be found.
- You will have to install RepoBee for your personal use, this is a one-time configuration and shouldn't take long. This can simply be done by following the steps detailed in their docs.
- Further configuration instructions have been provided in the `usage_guidelines` document. Some general configuration help can also be found on [RepoBee Docs](https://repobee.readthedocs.io/en/stable/userguide.html).

## GitLab access

- Finally, ensure that you have access to your GitLab account which you will register with RepoBee and use to store repositories remotely.
- You can [register](https://gitlab.com/users/sign_in) for a GitLab.com account, for free.
- **Note:** The 'Hidden Test' feature requires access to a machine with `gitlab-runner` installed and at least one 'Private' or 'Group' runner registered with the GitLab repositories which will run the the CI Pipeline Stage for hidden testing.
- **Note:** Any Pipeline stage using `checksum` comparisons also requires an external machine with the same set up as mentioned above.

## Install and configure GitLab Runner

- **(Required for 'Hidden Testing' and 'checksum comparison')**
- If you do not have access to a machine which can be used to install and run `gitlab-runner` then please remove any commands in the pipeline CI configuration file that require it. In the current templates the commands that require this can be recognised by the variable **\$STATIC**.
- **Note:** This can also easily be installed on your personal computer, doesn't need to be a cluster or server, as long as it has a reliable internet connection and appropriate permissions.
- Detailed instructions for installation of `gitlab-runner` can be found on the [official docs](https://docs.gitlab.com/runner/#install-gitlab-runner).
- Further details on its configuration can be found in the `configuring_runners` document.
