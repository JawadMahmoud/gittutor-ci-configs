# Configuring Runners

## Configuration

- Ensure that you have the `GitLab URL` and `Registration Token` for your Master Group. This can be found in the Runners section of the CI/CD settings of a group.
- The following command can then be used to register runners to your group:

```shell
sudo gitlab-runner register \
  --non-interactive \
  --url "GITLAB URL" \
  --registration-token "REGISTRATION TOKEN" \
  --executor "shell" \
  --description "PREFERRED NAME" \
  --tag-list "TAG1, TAG2" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```

- The capitalised sections will have to be replaced by your details.
- Ensure that you use the `shell` executor.
- Advanced configuration options can be found in the [official docs](https://docs.gitlab.com/runner/configuration/advanced-configuration.html).

## Registering Runners to `Master Group`

- Runners which are assigned to the Master Group can be utilized by every repository within the Group and its subgroups.
- This would avoid having to setup runners separately for each repository.
- Use job and runner [Tags](https://docs.gitlab.com/ee/ci/runners/#runner-runs-only-tagged-jobs) to classify which pipeline jobs should be run on which runners. More detailed information on how GitLab Pipelines use Tags can be found on there [Docs](https://docs.gitlab.com/ee/ci/yaml/README.html#tags).
- As soon as a Runner is configured as mentioned in the `Configuration` section, it will automatically appear on your GitLab group.
