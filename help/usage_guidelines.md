# Setting up the Assignment

Before starting to setup your assignments, ensure that you have either cloned or forked this repository and have a copy of it as a remote repository within your own GitLab profile as well. Ensure that any students can not edit the contents of this repository.

## Language set ups

The current templates utilise specific libraries and frameworks to provide essential functionality, it is recommended that these be used. If any additional libraries and functionality are to be added, please manually make changes to the CI configuration files as necessary. Kindly refer to the [GitLab CI Docs](https://docs.gitlab.com/ee/ci/) for understanding how to use CI YAML configurations.

### Python

The libraries used are the following: `Pylint` and `Pytest`

### Java

It is recommended that the project structure, which is also available as template in the `structure-templates/` directory, to be created and managed by using [Maven](https://maven.apache.org/what-is-maven.html).

## Creating the Master Group

You can use the guide provided by RepoBee to setup groups to ensure your configuration is valid, or follow the steps detailed below:

- Create a Group within your GitLab account (This will be referred to as the 'Master Group' in all guides.)
- The Master Group will be used for a particular 'Course' being taught.
- Every assignment to be distributed must have its sekeleton code as a repository within the Master Group.

## Configuring RepoBee

RepoBee has an extensive guide to help setting up the appropriate permissions and settings available in their docs. The overall steps required will be the following:

- Generate an Access Token for your GitLab profile with the permissions requested by RepoBee
- Run the following command and follow the steps of the wizard to setup your local RepoBee installation

```shell
repobee config-wizard
```

- Ensure that you include the `gitlab` plugin in the configuration of RepoBee.
- The `org_name` and the `master_org_name` will contain the same value, which will be the `slug` of name of the Master Group. This can be extracted from the URL of the Master Group's webpage.
- You can then verify whether your configuration of RepoBee is valid by executing

```shell
repobee verify-settings
```

- Only continue if RepoBee says the configuration is valid. Otherwise, you can run the wizard again or manually edit the RepoBee `config.cnf` file on your machine.
- A template `config.cnf` file can be found within the `help/` directory of this project.

## CI Configurations

### Runners

GitLab will by default enable Shared runners for executing pipelines for all repositories. A better solution is to have `gitlab-runner` installed and configured on a machine to provide private runners to your assignment groups.

### Generate CI configs

The script `scripts/generateyaml.py` provides the functionality to easily choose the langauge of the project and prompts the user to check which pipeline stages they would like to include within the generated pipeline configuration. After providing a few other details such as the `slug` of the assignment repository, the script generates the following files (if the assignment slug is `cs1ct-python-a`):

- `custom-configs/cs1ct-python-a.yml`
- `custom-configs/verify/cs1ct-python-a/.gitlab-ci.yml`

The first file contains the detailed implementation of the CI pipeline, whereas the second file is only a placeholder which should be edited by the user to link to the first file hosted on a server which is accessible.

The easiest option is to follow the strategy explained above by ensuring that a clone of this GitTutor Project is created under the profle of the instructor. The instructor can then us separate branches to store generated CI configurations, or use the same branch, whatever they prefer. The second file generated must be edited to link reotely to the first file. An example of this usecase is shown below:

```markup
include:
  remote: https://gitlab.com/JawadMahmoud/gittutor-ci-configs/-/raw/trial-configs/custom-configs/cs1ct-python-a.yml

```

This is necessary to ensure that the CI configuration file stored in the skeletion assignment repository (and hence student repositories), is the obscure version shown above. This file will also be used to validate the MD5 checksum of the students' CI configuration files when generating reports. Hence it is essential that this file be edited similar to the way shown above, linking to the other raw file generated during this process. This obscure CI config should then be copied into the skeleton repository.

The following variables will be prompted for input:

- `$TESTS_PATH` - The path to test files from the project root.
- `$STATIC_FILES` - The absolute path to the directory, on the machine running GitLab Runner, which stores hidden files.
- `$RESULTS_PATH` - The path to create the results artefacts from the project root.
- `$PROJECT_NAME` - The slug of the project name.

**Note:** Kindly review the commands executed using these variables to completely grasp how they are utilised. Further changes can be made manually as well.

## Hidden tests and validation

Some of the pipeline stages allow the user to implement hidden testing strategies. For this to be usable, the user must store these hidden files on the machine where `gitlab-runner` has been registered to run jobs for that particular assignment. These hidden tests directories are requested from the user using prompts.

Similarly, for validation of the integrity of the test files already included in the skeleton repository, it is important to store checksums of the original versions on the machine and compare them against the students' test files to check whether any changes were made.

- All files for validation for a porject (stored on the machine running `gitlab-runner`), in the current templates, are stored in the `$STATIC_FILES/$PROJECT_NAME` directory.
- All hidden test files are similarly placed in the `$STATIC_FILES/$PROJECT_NAME/$TESTS_PATH` directory.

Validation of test files is performed using `MD5` checksums. It is important to generate a `checksum.txt` file and store it on the runner machine in the `$STATIC_FILES/$PROJECT_NAME` directory. An example of this is shown below:

```shell
## Create MD5 checksum and store it in a file called checksum.txt
## Replace all parts enclosed by {}
md5sum {file_path/file_name} {file_path/file_name} > {output_path/checksum.txt}
```

## Setting up student repositories

- Once the configuration has been validated, ensure that the skeleton assignment repository has been pushed to the same Master Group used to configure RepoBee.
- Project structure templates can be found in the `structure-templates/` directory, these can be used to as the basis of your skeleton code to avoid having to make to many changes to other configuration files.
- Student repositories can be setup by provided RepoBee with two parameters:
  - The `slug` of skeleton repository
  - The list of student IDs (GitLab User IDs) to create repositories for.
- The student list can be entered in different ways when running the RepoBee command to generate groups and cloned repositories for all students

```shell
## Space-separated student IDs
repobee setup --mn skeleton-repo-slug -s studentA studentB
## Text file with one student ID on each line
repobee setup --mn skeleton-repo-slug --sf students.txt
```

- Student repositories are created within subgroups created for each student, all of which are contained within the Master Group.
- For more advanced usage features of RepoBee, kindly refer to their docs.

## Generating Reports

Similar to the other script, the `scripts/getreports.py` prompts the user for several inputs to check which assignment the user wishes to generate reports for, it also requires a GitLab Access Token to be able to access the GitLab API and read repositories owned by the user.

This script then outputs several folders and files, which include detailed latest pipeline output reports generated by each students' CI pipeline, summaries of student performances over different stages of the pipeline and any warnings for students who may have tried to modify crucial files like the CI configuration YAML file.
