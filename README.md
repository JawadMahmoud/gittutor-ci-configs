# GitTutor CI Configs

The project artifacts for the GitTutor system which allow generation of personlised CI configuration files. The project currently focuses on providing templates and procedures only for Python and Java languages.

Usage guidance and other details can be found in the `help/` directory.

Kindly read the guides in the following order:

- `help/usage_requirements.md`
- `help/configuring_runners.md`
- `help/usage_guidelines.md`

The current functionality available using templates is shown in the table below:

| Functionality     |  Languages   |
| ----------------- | :----------: |
| Test Validation   |    Python    |
| Report Generation | Python, Java |
| Code Quality      |    Ptyhon    |
| Module Limitation |    Python    |
| Build Test        |     Java     |
| Unit Testing      | Python, Java |
| Hidden Testing    |    Python    |
| Test Generation   |     None     |
| Time Complexity   |     None     |
